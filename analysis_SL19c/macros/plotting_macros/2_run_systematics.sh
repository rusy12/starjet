#!/bin/bash
#this macro produces root files containing TGraphs of final results with several kinds of systematic errors
source ../set_paths.sh

EVO="GPC2" #dataset label
TOYMODEL=1 #input type; 0 - real data, 1 - toymodel simulation
PPBASE="PYTHIA" #PYTHIA | STAR
#TEST=1 # 0:chi2 1:Camberra 2: Kolmogorov-Smirnov 

UNFOLDED_DATA="$ANALYSISDIR/out/MB"
if [ $TOYMODEL -eq 1 ]; then
	UNFOLDED_DATA="$TOYMODELDIR/DataOut/pythia/jet_plus_bg" 
fi

INDIR="$UNFOLDED_DATA" #input directory with unfolded data and unfolding QA files
OUTDIR="$ANALYSISDIR/macros/plotting_out" #output directory
PYTHIADIR="$ANALYSISDIR/macros/pp_baseline/PYTHIA6_Jana" #directory with pp spectrum for RAA


SCRIPT_DIR="0_produce_input_data/systematics"
cd $SCRIPT_DIR

for PPBIAS in 0 #1: use pythia with pTlead cut as the pp baseline for RAA | 0: use PYTHIA with no pTlead cut
do
if [ $PPBIAS -eq 0 ]; then
	SUFFIX2=${EVO}
else
	SUFFIX2="${EVO}_ppBiased"
fi


for BININGCH in 1 #0 4 #1 2 3 4 #2 #binning set
do
	for SYSTEM in cent #peri #pp # 
do
	TOYDIR=""
	if [ $SYSTEM == cent ]; then #central collisions
		PTLEADCUTS="5 6 7"
		PTLEADCUTS_DENOM="5"
		RATIOS="RAA RR plpl RCP"
		if [ $TOYMODEL -eq 1 ]; then
		RATIOS="closure RAA RR plpl"
		TOYDIR="toymodel/"
		fi
		CENTDIR="central"
	elif [ $SYSTEM == peri ]; then #peripheral collisions
		PTLEADCUTS="4 5 6 7"
		PTLEADCUTS_DENOM="4 5 6"
		RATIOS="RAA RR plpl"
		CENTDIR="peripheral"
	else #pp data
		PTLEADCUTS="5" #"4 5 6" 
		PTLEADCUTS_DENOM="4"
		RATIOS="RAA RR plpl"
		CENTDIR="pp"
	fi
	UNCERTAINTIES="unfolding correlated"  #normalization
	if [ $TOYMODEL -eq 1 ]; then
		UNCERTAINTIES="unfolding"
	fi

	#create output directories
	OUTDATA="$OUTDIR/systematics/${TOYDIR}${CENTDIR}/$SUFFIX2/bining$BININGCH"
	OUTFIG="$OUTDIR/figures/sys_errors/bin$BININGCH"
	mkdir -p $OUTDATA
	mkdir -p $OUTFIG
	echo "WARNING: Removing files in $OUTDATA"
	rm $OUTDATA/*.root

	for RAT_T in `echo $RATIOS` 
	do
	for R in 0.2 0.3 0.4 #0.5
	do
	for PTLEAD in `echo $PTLEADCUTS`
	do
	for PTLEAD_DENOM in `echo $PTLEADCUTS_DENOM`
	do
	for UNC_T in `echo $UNCERTAINTIES`
	do
		root -q -l -b systematics.C\($UNC_T,$RAT_T,$SYSTEM,$R,$PTLEAD,$PTLEAD_DENOM,\"$EVO\",$BININGCH,\"normal\",$PPBASE,$PPBIAS,$TOYMODEL,1,0,\"$INDIR\",\"$OUTDIR\",\"$PYTHIADIR\"\)
	done #uncert
	done #pTlead_denom
	done #pTlead
	done #R
	done #ratio
done #centrality
done #bining
done #pythia bias
