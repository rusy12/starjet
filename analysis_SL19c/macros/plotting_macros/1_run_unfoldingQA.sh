#!/bin/bash
#SCRIPT1 calculates distances between iterations of unfolded solutions, between measured and backfolded spectrum and saves the results
#SCRIPT2 uses results fo SCRIPT1 to determine best iterations and saves the output to text files, which are then readed by the code in the "systematics" subdirectory

source ../set_paths.sh

EVO="GPC2" #version label of the unfolded results
DO_TOYMODEL=0 #input type; 0 - real data, 1 - toymodel simulation
RUN_QA=1 #run SCRIPT1 (unfolding QA); the QA is quite slow and it is better to use the submitter script and run it as a job rather than run it here
RUN_BESTITER=1 #run SCRIPT2 (based on the unfolding QA finds the best iterations)

UNFOLDED_DATA="$ANALYSISDIR/out/MB"
if [ $DO_TOYMODEL -eq 1 ]; then
	UNFOLDED_DATA="$TOYMODELDIR/DataOut/pythia/jet_plus_bg" 
fi

INPUT1="$UNFOLDED_DATA" #input directory for SCRIPT1
OUTPUT1="$UNFOLDED_DATA" #output directory for SCRIPT1
#OUTPUT1="./QA_test" #output directory for SCRIPT1
INPUT2="$UNFOLDED_DATA" #input directory for SCRIPT2 (usually the same as $OUTPUT1)
OUTPUT2="../../utils/optIter" #output directory for SCRIPT2
SCRIPT1="unfolding_qa"
SCRIPT2="find_opt_iter"
SCRIPT_DIR="./0_produce_input_data/unfoldingQA"

cd $SCRIPT_DIR

for CENTRALITY in peri cent
do
for BINNING in 1 #0 4
do

#create output directory and its substructure in case it is different than the input directory
if [ $INPUT1 != $OUTPUT1 ]; then
   #echo "creating directory: "
	if [ $CENTRALITY == "cent" ]; then
		CCLASS="central"
	else
		CCLASS="peripheral"
	fi
	for RPARAM in 0.2 0.3 0.4
	do
	for UNFOLDING in "SVD" "Bayes"
	do
	for PRIOR in "pythia" "powlaw3" "powlaw45" "powlaw5" "powlaw55" "tsalis_1" "tsalis_2" "tsalis_3" "tsalis_4" "tsalis_5" "tsalis_6" "tsalis_7" "tsalis_8" "tsalis_9"
	do
	for SYSDATA in "normal" "pythiaUG" "RRho02" "RRho04" "nrem-1" "pp" "v2" "m5" "p5" "u" "g"
	do
		if [ $SYSDATA == "normal" ] || [ $SYSDATA == "pp" ] || [ $SYSDATA == "v2" ] || [ $SYSDATA == "m5" ] || [ $SYSDATA == "p5" ] || [ $SYSDATA == "u" ] || [ $SYSDATA == "g" ]; then
			SYSSUFF1="main"
		else
			SYSSUFF1=$SYSDATA
		fi
		#echo "inclusive_${CCLASS}_${SYSSUFF1}/Unfolded_R${RPARAM}_${UNFOLDING}_VARbins_bining${BINNING}_BGD_${EVO}_${SYSDATA}/$PRIOR"
		mkdir -p "$OUTPUT1/inclusive_${CCLASS}_${SYSSUFF1}/Unfolded_R${RPARAM}_${UNFOLDING}_VARbins_bining${BINNING}_BGD_${EVO}_${SYSDATA}/$PRIOR"
	done #systematic uncertainty type
	done #prior loop
	done #unfolding
	done #R
fi #creating output directories	

if [ $RUN_QA -eq 1 ]; then
root -l -b <<EOF  
.L ${SCRIPT1}.C
${SCRIPT1}($CENTRALITY, $BINNING,  "$EVO", "$INPUT1","$OUTPUT1",$DO_TOYMODEL,0)
.q
EOF
fi

if [ $RUN_BESTITER -eq 1 ]; then
root -l -b <<EOF
.L ${SCRIPT2}.C
${SCRIPT2}($CENTRALITY,$BINNING, $DO_TOYMODEL, "$EVO", "$INPUT2", "$OUTPUT2")
.q
EOF
fi
done #binning
done #centrality
