#ifndef StJetAna_h
#define StJetAna_h

#include <bitset>
#include "StRoot/StPicoEvent/StPicoDst.h"
#include "StRoot/StPicoEvent/StPicoDstReader.h"
#include "StRoot/StPicoEvent/StPicoEvent.h"
#include "StRoot/StPicoEvent/StPicoTrack.h"
#include "StRoot/StRefMultCorr/StRefMultCorr.h"
#include "StRoot/StRefMultCorr/CentralityMaker.h"
#include "StarRoot/StMemStat.h"
/*
#include "StRoot/StJetPico/TStarJetPicoReader.h"
#include "StRoot/StJetPico/TStarJetPicoEvent.h"
#include "StRoot/StJetPico/TStarJetPicoEventHeader.h"
#include "StRoot/StJetPico/TStarJetPicoEventCuts.h"
#include "StRoot/StJetPico/TStarJetPicoPrimaryTrack.h"
#include "StRoot/StJetPico/TStarJetPicoTrackCuts.h"
#include "StRoot/StJetPico/TStarJetPicoUtils.h"
*/
#include "TDatime.h"
#include "StThreeVectorF.hh"
#include "StMaker.h"
#include "TH1F.h"
#include "TF1.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TFile.h"
#include "TChain.h"
#include "TSystem.h"
#include "TRandom.h"
#include "TLorentzVector.h"
#include "TNtuple.h"
#include "TPythia6.h"
#include "TParticle.h"
#include "TParticlePDG.h"

//FastJet 3
#include "fastjet/config.h"
#include "fastjet/PseudoJet.hh"
#include "fastjet/JetDefinition.hh"
#include "fastjet/ClusterSequence.hh"
#include "fastjet/ClusterSequenceArea.hh"
#include "fastjet/Selector.hh"
#include "fastjet/tools/Subtractor.hh"

class StPicoDst;
class StPicoDstReader;

using namespace std;
using namespace fastjet;

#include "fastjet/tools/JetMedianBackgroundEstimator.hh"


class StJetAna : public StMaker {
  public:
    StJetAna(const char *name, StPicoDstReader *picoReader/*, const char *outName*/);
    StJetAna();
    virtual ~StJetAna();
    
    virtual Int_t Init();
    virtual Int_t Make_pico(int event=0);
    //virtual Int_t Make_Janpico(int nev);
    virtual void  Clear(Option_t *opt="");
    virtual Int_t Finish();
    
    void    DeclareHistograms(bool embedding, bool qa);
    void    WriteHistograms(bool embedding, bool qa);
    void    FillJetHistos(float pT_jet, float pTcorr_jet, float area_jet, float pTlead, float eta_jet, float phi_jet, int r, int nparticles, float weight=1.0);
	 void 	FillJetHistosEmb(float pTcorr_jet, float area_jet, float pTlead, float pT_emb,int r, float weight=1.0/*,float eta_jet, float phi_jet, float eta_emb,float phi_emb*/);
	 void		FillEventHistos(float zvertex, float zVPD, float ranking, int refmult, int day, int eventid, int runid, float bbcrate, int *ntracks, float weight=1.0);
	 void 	FillQAHistos(float track_pT, float track_eta, float track_phi, float track_pT_glob, float track_eta_glob, float track_phi_glob,float dca, float dcaXY, float chi2, int npoints, int npointsMax, float zVertex, float zVPD, bool TTOF_match, float weight=1.0);
	 void 	JetReco(vector<PseudoJet> input_vector_data, float R, short Remove_N_hardest, float weight=1.0, short embedding=-1);
	 vector<PseudoJet> EmbedJet(short jetType, float &pT_emb, float R, vector<PseudoJet> container/*, float* eta_emb, float* phi_emb*/, TString parton="u", TPythia6* pythia=NULL);
	 bool 	FindEmbeddedJet(vector<PseudoJet> constituents,float pT_emb);
	 bool 	BadRun(int id);
	 bool 	BadDay(int day, short run=12);


  private:
   StPicoDstReader *mPicoDstReader;
   StPicoDst      *mPicoDst;
	StMemStat memstat;
    
	TFile* fOutFile;
   //TString    mOutName;
	TString sInFileList;
	TString sInputdir;
	TString sTrigger;
	TString sParton;
	short kDoEmbedding;
	bool kDoQA;
	bool kSaveTree;
	bool kDoAuAu;
	bool kDoEventCuts;
	bool kreqTOF;
	bool kGlobal;
	bool kPythiaEmb;
   float fR;
	float fAcut;
   float fMaxRap;
   float fDCACut;
   float fDCAxyCut;
   float fChi2Cut;
   float fZVertexCut;
   float fDeltaZ;
   float fR_bg;
	float fpTminCut;	
	float fpTmaxCut;	
   float fFitOverMaxPointsCut;
	float fGhost_maxrap;
	float fZvertex;
	float fZvpd;
	float fRefMultCor;
	float fRefMult;
	int nRunID;
	int npTlead;
	int nRefmultcutMin;
   int nRefmultcutMax;
   int nJetsRemove;
	int nSigmaCut;
	int nFitPointsCut;
	int nEmb;
	int nR;
	int nBBCmax;
	int nBBCmin;
	//track TTree variables 
	float fpT_glob;
	float fpT_prim;
	float feta_glob;
	float feta_prim;
	float fphi_glob;
	float fphi_prim;
	float fDCAg;
	float fDCAxy;
	float fDCAz;
	float fchi2;
	int nNfit;
	int nNfitNmax;
	
	TPythia6* fpythia;

	//event histograms
	TTree *fEventTree;
	TH1D *hrefmult;
	TH2D *hrefmult_weight;
	TH1D *hweight;
	TH1D *hevents;
	TH1D *hevents_nw;
	TH1D *heventid;
	TH1D *hzvertex;
	TH1D *hdeltaz;
	TH2D* hz_refmult;
	TH2D* hday_refmult;
	TH2D* hrunid_refmult;
	TH2D* hday_z;
	TH2D* hbbcrate_ntr;
	TH1D* hmeanrefmult;
	TH1D* hmeanz;
	TH1D* hranking;
	TH1D* hntr;
	TH1D* hntr_1gev;
	TH1D* hntr_5gev;
	TH1D* hntr_7gev;

	//jet histograms
	TH2D *hpT_pTlead[4];
	TH2D *heta_phi[4];
	TH1D *hjetarea[4];
	TH1D *hjetarea_cut[4];
	TH2D *hjetpTarea[4];
	TH2D *hjetpTcorrArea[4];
	TH2D *delta_pt_BG_sp[15][4];
	TH1D *hrho[4];
	TH1D *hpT_pTl[15][4];	
	TH2D *hjetstructure[4];
	TH2D *hnparticlesinjets[4];
	TH2D *hjetpTembArea[4];

	//track histograms
	TTree *fTrackTree;
	TH1D *hpT_tr;
	TH1D *hpT_tr_glob;
	TH2D* hpT_prim_glob;
	TH1D* hpT_prim_over_glob;
	TH1D *hTOFmatch;
	TH2D *htrNpoints;
	TH2D *htrNpoints_1gev;
	TH2D *htrNpoints_5gev;
	TH2D *htrNpoints_7gev;
	TH2D *heta_phi_tr;
	TH2D *heta_phi_tr_glob;
	TH2D *heta_phi_tr1gev;
	TH2D *heta_phi_tr5gev;
	TH2D *heta_phi_tr7gev;
	TH2D *hdca_z_tr;
	TH2D *hdca_pT;
	TH1D *hdca_tr;
	TH1D *hdcaXY_tr;
	TH1D *hchi2_tr;

    ClassDef(StJetAna, 1)
};

#endif

