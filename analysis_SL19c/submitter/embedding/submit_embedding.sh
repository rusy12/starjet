#!/bin/bash
export SCRIPTNAME="run_embedding.csh"
export GLOBAL=0 #primary or global tracks
export NFIT=12 #number of fit points (used only for output file description)

for PARTICLE in "Pi" "P" "K" 
do
export PARTICLE 
export FILELIST="filelists/${PARTICLE}_file.list" 
	for CENTRAL in 1 0 
	do
		export CENTRAL
		condor_submit embedding.job
	done
done
