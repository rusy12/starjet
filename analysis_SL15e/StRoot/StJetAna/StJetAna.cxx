#include "StJetAna.h"

static StRefMultCorr* refmultCorrUtil;
static float fEmbPt[] =  {1.0, 2.0, 3.0, 4.0, 5.0,6.0, 7.0, 8.0, 10.0, 20.0, 40.0, 90.0}; //pT of embedded jets
static float fRpar[] = {0.2,0.3,0.4,0.5};
static float fAcuts[] = {0.07,0.2,0.4,0.65}; //jet area cuts
static char* ALEX_EVENT_TREE   = "JetTrackEvent";
static char* ALEX_EVENT_BRANCH = "Events";


ClassImp(StJetAna)

//-----------------------------------------------------------------------------
StJetAna::StJetAna(const char* name, StPicoDstMaker *picoMaker/*, const char* outName*/)
  : StMaker(name)
{
  mPicoDstMaker = picoMaker;
  mPicoDst = 0;
}

//----------------------------------------------------------------------------- 
StJetAna::StJetAna()
{
}

//----------------------------------------------------------------------------- 

StJetAna::~StJetAna()
{ /*  */ }

//----------------------------------------------------------------------------- 
Int_t StJetAna::Init() {
	//several parameters are passed via system variables 
	//these are defined in the runing macro (e.g. run.sh)

	//settings
	sTrigger=gSystem->Getenv("TRIGGER");
	cout<<"Trigger:"<<sTrigger<<endl;
	kDoAuAu = atoi(gSystem->Getenv("DOAUAU")); //Au+Au or p+p collisions
	kDoEmbedding = atoi(gSystem->Getenv("DOEMBEDDING")); //run embedding for delta-pT calculation
	kDoQA= atoi(gSystem->Getenv("DOQA")); //run only event/track QA
	kSaveTree= atoi(gSystem->Getenv("SAVETREE")); //save ttree with event info
	kDoEventCuts=atoi(gSystem->Getenv("DOEVENTCUTS")); //apply event cuts
	//parameters
   //fR = atof(gSystem->Getenv("RPARAM")); //jet resolution parameter
   fR_bg = atof(gSystem->Getenv("RRHO"));//R for kt jets (for rho calculation)
   nJetsRemove=atoi(gSystem->Getenv("NJETSREMOVE")); //remove n hardest jets from rho calculation
	nR=4;  //number of jet size parameters R
	npTlead=10; //number of pTleading cuts
	nEmb=12;  //number of embedded probes
	//event cuts
   nRefmultcutMin = atoi(gSystem->Getenv("REFMULTMIN")); //minimal corrected reference multiplicity
	nRefmultcutMax = atoi(gSystem->Getenv("REFMULTMAX")); //maximal -||-
   fZVertexCut= atof(gSystem->Getenv("ZVERTEX")); //30.0 | maximal z of primary vertex
   fDeltaZ= atof(gSystem->Getenv("ZTPCZVPD")); //4.0 | maximal |zTPC-zVPD| in pp collisions - for pile-up removal
	//track cuts	
	kGlobal=atoi(gSystem->Getenv("GLOBAL")); //use global or primary tracks
	kreqTOF=atoi(gSystem->Getenv("TOFBEMC")); //true | require match in BEMC or TOF for pp collisions
   fMaxRap = atof(gSystem->Getenv("MAXRAP")); //1.0 | maximum track rapidity range
   fDCACut = atof(gSystem->Getenv("DCA")); //1.0 | minimal track DCA
   fChi2Cut = atof(gSystem->Getenv("CHI2")); //1000(=no cut) | chi2 of the track fit
   nBBCmin = atoi(gSystem->Getenv("BBCMIN")); //0 (=no cut) | minimal BBC rate for pp collisions
   nBBCmax = atoi(gSystem->Getenv("BBCMAX")); //1E7 (=no cut) | maximal BBC rate for pp collisions
	fpTminCut=0.2;//min track pT
	fpTmaxCut=30; //max track pT
	nSigmaCut=3; //nsigma cut for dEdx particle identification
	nFitPointsCut=atoi(gSystem->Getenv("NFIT"));//15 | minimal number of TPC hits
	fFitOverMaxPointsCut=atof(gSystem->Getenv("NFITNMAX")); //0.55 | # of TPC hits / # of possible TPC hits 
	//jet cuts
	//fAcut= atof(gSystem->Getenv("ACUT")); //jet area cut
	fGhost_maxrap = 1.0; // Fiducial eta cut for background estimation
	//embedding settings
	kPythiaEmb=atoi(gSystem->Getenv("PYTHIAEMB"));	//embed pythia jets instead of single particle
	sParton=gSystem->Getenv("PARTON"); //fragment u quark or gluon?
	
	//output file
	//TString outPath=gSystem->Getenv("OUT_PATH");
	TString outPath=gSystem->Getenv("SCRATCH");
	TString outId=gSystem->Getenv("JOBID");
	//TString outFile=Form("histos_inclusivejet_R%.1lf.root",fR);
	TString outFile=Form("histos_inclusivejet_%s.root",outId.Data());
	if(kDoEmbedding)
		outFile=Form("histos_embeddedjet_%s.root",outId.Data());
	if(kDoQA)
		outFile=Form("qa_%s.root",outId.Data());
  	TString outName = Form("%s/%s",outPath.Data(),outFile.Data());
	cout<<"creating output file "<<outName.Data()<<endl;
	fOutFile = new TFile(outName.Data(),"RECREATE");
   //	fOutFile->cd();
	if(fOutFile->IsOpen())cout<<"OUTPUT FILE IS OPEN"<<endl;
	else cout<<"OUTPUT FILE IS NOT OPEN!!!"<<endl;

	//declare histograms
	DeclareHistograms(kDoEmbedding,kDoQA);

	//refmultCorrUtil instance
	if(kDoAuAu)
	  	refmultCorrUtil = CentralityMaker::instance()->getRefMultCorr();
	
	//for Alex's and Jan's picoDsts
	//sInFileList=gSystem->Getenv("FILELIST");
	//sInputdir="root://pstarxrdr1.nersc.gov//star/picodsts/aschmah/Jet/AuAu200_run11//histo_out_V2/mode_1/";
	
	//initialize TPythia6 for pythia jet embedding
	if(kDoEmbedding)
	{
		//Setting random seed
		TDatime dt;
	  	UInt_t curtime = dt.Get();
		UInt_t procid = gSystem->GetPid();
  		UInt_t seed = curtime - procid;
  		gRandom->SetSeed(seed);

		//PYTHIA
		fpythia = new TPythia6();
		fpythia->SetMRPY(1, seed);
	}

  memstat.Start();

  return kStOK;
}

//----------------------------------------------------------------------------- 
Int_t StJetAna::Finish() {
	 
    fOutFile->cd();
    WriteHistograms(kDoEmbedding,kDoQA);
    fOutFile->cd();
	 if(kDoQA && kSaveTree) 
	 {
		fEventTree->Write();
		fTrackTree->Write();
	 }
    fOutFile->Close();

	 memstat.Stop();
  
  return kStOK;
}

//-----------------------------------------------------------------------------
void StJetAna::DeclareHistograms(bool embedding, bool qa) {
	
//histogram definitions
  int npTleadbins=25;
  float pTleadmin=0;
  float pTleadmax=25;
  Int_t nptbins=800;
  Float_t ptminbin=-100;
  Float_t ptmaxbin=100;
  Int_t netabins=100*2;
  Float_t etaminbin=-1;
  Float_t etamaxbin=1;
  Int_t nphibins=120;
  Float_t phiminbin=0;//-TMath::Pi();
  Float_t phimaxbin=2*TMath::Pi();
  int refmultbins=130;
  float refmultmin=0;
  float refmultmax=650;
	if(!kDoAuAu)
	{
		refmultbins=50;
		refmultmin=0;
		refmultmax=50;
	}
	int daybins=250;
	float daymin=0.5;
	float daymax=250.5;
	int runbins=100000;
	float runmin=0;
	float runmax=100000;
	int zbins=50;
	float zmin=-50;
	float zmax=50;
	int npttrackbins=120;
	float pttrackmin=0;
	float pttrackmax=30;
	int ntrackbins=100;
	int maxtracks=1000;
	if(!kDoAuAu)
   {
		 ntrackbins=25;
		 maxtracks=25;
	}
	
	//event histograms
	hevents = new TH1D("hevents", "number of events", 2, 0, 2);
	hevents_nw = new TH1D("hevents_noweight", "number of events, unweighted", 2, 0, 2);
	heventid = new TH1D("heventid", "event id", 1000,400000, 401000);
	hrefmult=new TH1D("hrefmult", "ref. multiplicity",refmultbins,refmultmin,refmultmax);
	hrefmult_weight=new TH2D("hrefmult_weight", "ref. multiplicity vs weight",refmultbins,refmultmin,refmultmax,100,0,2);
	hweight=new TH1D("hweight", "weight",100,0,2);
	hzvertex=new TH1D("hzvertex", "z-position of primary vertex", zbins, zmin, zmax);
	hdeltaz=new TH1D("hdeltaz", "zTPC-zVPD; #Delta [cm]", 80,-10,10);
	hz_refmult=new TH2D("hz_refmult", "zvertex vs refmult; z [cm]; refMult", zbins,zmin,zmax, refmultbins,refmultmin,refmultmax);
	hday_refmult=new TH2D("hday_refmult", "day vs refmult; day; refMult", daybins, daymin, daymax, refmultbins, refmultmin, refmultmax);
	//hrunid_refmult=new TH2D("hrunid_refmult", "runid vs refmult; runid; refMult", runbins, runmin,runmax, refmultbins, refmultmin, refmultmax);
	hday_z=new TH2D("hday_z", "day vs z; day; z", daybins, daymin, daymax, zbins, zmin, zmax);
	hbbcrate_ntr=new TH2D("hbbcrate_ntr", "bbc rate vs #of accepted tracks; bbc rate; # of tracks",300,0,3000000,ntrackbins,0,maxtracks);
	hntr=new TH1D("hntr", "#of accepted tracks; # of tracks; counts",100,0,1000);
   hntr_1gev=new TH1D("hntr_1gev", "#of accepted tracks with pT>1GeV; # of tracks; counts",100,0,1000);
   hntr_5gev=new TH1D("hntr_5gev", "#of accepted tracks with pT>5GeV; # of tracks; counts",100,0,1000);
   hntr_7gev=new TH1D("hntr_7gev", "#of accepted tracks with pT>7GeV; # of tracks; counts",100,0,1000);
	hmeanrefmult=new TH1D("hmeanrefmult", "<refmult> vs day; day; <refmult>",daybins, daymin, daymax);
	hmeanz=new TH1D("hmeanz", "<z> vs day; day; <z>",daybins, daymin, daymax);
	hranking=new TH1D("hranking","PV ranking",200,0,2000);

	//event info tree
	fEventTree = new TTree("eventTree","event properties");
	TBranch *br_runid = fEventTree->Branch("runid", &nRunID);
	TBranch *br_refmult = fEventTree->Branch("refmult", &fRefMult);
	TBranch *br_refmultcor = fEventTree->Branch("refmultcor", &fRefMultCor);
	TBranch *br_zvertex = fEventTree->Branch("zvertex", &fZvertex);
	TBranch *br_zvpd = fEventTree->Branch("zVPD", &fZvpd);


	if(qa) //only track and event QA
	{
		hpT_prim_glob = new TH2D("hpT_prim_glob", "primary track pT vs global track pT;p_{T}^{prim};p_{T}^{glob}", npttrackbins,pttrackmin,pttrackmax,npttrackbins,pttrackmin,pttrackmax);
		hpT_prim_over_glob = new TH1D("hpT_prim_over_glob", "primary track pT / global track pT;p_{T}^{prim}/p_{T}^{glob};counts", 20,0,2.0);
		heta_phi_tr = new TH2D("heta_phi_tr", "primary track eta vs phi;#eta;#phi", netabins, etaminbin, etamaxbin, nphibins, phiminbin, phimaxbin);
		heta_phi_tr_glob = new TH2D("heta_phi_tr_glob", "global track eta vs phi;#eta;#phi", netabins, etaminbin, etamaxbin, nphibins, phiminbin, phimaxbin);
		heta_phi_tr1gev = new TH2D("heta_phi_tr1gev", "track eta vs phi for tracks with pT>1GeV;#eta;#phi", netabins, etaminbin, etamaxbin, nphibins, phiminbin, phimaxbin);
		heta_phi_tr5gev = new TH2D("heta_phi_tr5gev", "track eta vs phi for tracks with pT>5GeV;#eta;#phi", netabins, etaminbin, etamaxbin, nphibins, phiminbin, phimaxbin);
		heta_phi_tr7gev = new TH2D("heta_phi_tr7gev", "track eta vs phi for tracks with pT>7GeV;#eta;#phi", netabins, etaminbin, etamaxbin, nphibins, phiminbin, phimaxbin);
		htrNpoints = new TH2D("htrNpoints", "N fit points vs N max points;#N fit;#N max", 80, 0.5, 80.5, 90, 0.5,90.5);
		htrNpoints_1gev = new TH2D("htrNpoints_1gev", "N fit points vs N max points for tracks with pT>1GeV;#N fit;#N max", 40, 0.5, 40.5, 90, 0.5,90.5);
		htrNpoints_5gev = new TH2D("htrNpoints_5gev", "N fit points vs N max points for tracks with pT>5GeV;#N fit;#N max", 40, 0.5, 40.5, 90, 0.5,90.5);
		htrNpoints_7gev = new TH2D("htrNpoints_7gev", "N fit points vs N max points for tracks with pT>7GeV;#N fit;#N max", 40, 0.5, 40.5, 90, 0.5,90.5);
		hpT_tr=new TH1D("hpT_tr", "primary track pT; p_{T} [GeV/c]", npttrackbins,pttrackmin,pttrackmax);
		hpT_tr_glob=new TH1D("hpT_tr_glob", "global track pT; p_{T} [GeV/c]", npttrackbins,pttrackmin,pttrackmax);
		hdca_z_tr=new TH2D("hdca_z_tr", "track DCA vs z-vertex", 30,0,3,zbins,zmin,zmax);
		hdca_tr=new TH1D("hdca_tr", "track DCA", 50,0,5);
		hchi2_tr=new TH1D("hchi2_tr", "track fit chi2", 25,0,10);
		hdca_pT=new TH2D("hdca_pT", "track DCA vs. p_{T}", 30,0,3, npttrackbins,pttrackmin,pttrackmax);
		hTOFmatch=new TH1D("hTOFmatch", "pT of tracks with (TOFvBEMC) match; track p_{T} [GeV/c]",npttrackbins,pttrackmin,pttrackmax);

		//track info tree
		fTrackTree = new TTree("trackTree","track properties");
		TBranch *br_GpT = fTrackTree->Branch("pT_global", &fpT_glob);
		TBranch *br_PpT = fTrackTree->Branch("pT_primary", &fpT_prim);
		TBranch *br_Geta = fTrackTree->Branch("eta_global", &feta_glob);
		TBranch *br_Peta = fTrackTree->Branch("eta_primary", &feta_prim);
		TBranch *br_Gphi = fTrackTree->Branch("phi_global", &fphi_glob);
		TBranch *br_Pphi = fTrackTree->Branch("phi_primary", &fphi_prim);
		TBranch *br_DCAg_old = fTrackTree->Branch("DCAg_old", &fDCAg_old);
		TBranch *br_DCAg_new = fTrackTree->Branch("DCAg_new", &fDCAg_new);
		TBranch *br_DCAxy = fTrackTree->Branch("DCAxy", &fDCAxy);
		TBranch *br_DCAz = fTrackTree->Branch("DCAz", &fDCAz);
		TBranch *br_chi2 = fTrackTree->Branch("chi2", &fchi2);
		TBranch *br_nfit = fTrackTree->Branch("Nfit", &nNfit);
		TBranch *br_nfitnmax = fTrackTree->Branch("NfitNmax", &nNfitNmax);
	}

	if(embedding) //simulated jets => delta-pT histograms
	{
		for(int r=0; r<nR; r++)
		{
		TString histn=Form("hjetpTembArea_R0%0.lf",fRpar[r]*10);
		hjetpTembArea[r]=new TH2D(histn,"jet pTemb vs area",nptbins, ptminbin, ptmaxbin,100,0,1);
		for(Int_t pTlcut=0; pTlcut<npTlead; pTlcut++)
		{
			TString hname=Form("delta_pt_BG_sp_%i_R0%0.lf", pTlcut, fRpar[r]*10);
 	   	delta_pt_BG_sp[pTlcut][r] = new TH2D(hname,"delta pT for BG corrections, using sp probe", nptbins, ptminbin, ptmaxbin, nptbins, ptminbin, ptmaxbin);
		}	
		}
	}

	if(!embedding && !qa) //jets in real data
	{
		for(int r=0; r<nR; r++)
		{
		TString hname=Form("hpT_pTlead_R0%.0lf",fRpar[r]*10);
		hpT_pTlead[r] = new TH2D(hname, "jet pTcorr vs pTleading; p_{T} [GeV/c]; p_{T}^{lead} [GeV/c]", nptbins, ptminbin, ptmaxbin, npTleadbins, pTleadmin,pTleadmax);

		hname=Form("heta_phi_R0%.0lf",fRpar[r]*10);
		heta_phi[r] = new TH2D(hname, "jet eta vs phi;#eta;#phi", netabins, etaminbin, etamaxbin, nphibins, phiminbin, phimaxbin);
  
		hname=Form("hjetarea_cut_R0%.0lf",fRpar[r]*10);
		hjetarea_cut[r] = new TH1D(hname,"jet area after cut",100,0,1);
		hname=Form("hjetarea_R0%.0lf",fRpar[r]*10);
		hjetarea[r] = new TH1D(hname,"jet area",100,0,1);
		hname=Form("hjetpTarea_R0%.0lf",fRpar[r]*10);
	   hjetpTarea[r] = new TH2D(hname,"jet pTmeasured vs area",nptbins, ptminbin, ptmaxbin,100,0,1);
		hname=Form("hjetpTcorrArea_R0%.0lf",fRpar[r]*10);
	   hjetpTcorrArea[r] = new TH2D(hname,"jet pTreco vs area",nptbins, ptminbin, ptmaxbin,100,0,1);
		hname=Form("hrho_R0%.0lf",fRpar[r]*10);
	   hrho[r] = new TH1D(hname,"rho",50,0,50);
		hname=Form("hjetstructure_R0%.0lf",fRpar[r]*10);
		hjetstructure[r] = new TH2D(hname,"jet constituents pT vs pTlead ; p_{T}^{part} [GeV/c]; p_{T}^{lead} [GeV/c]", 50, 0, 25,npTleadbins, pTleadmin,pTleadmax);
		hname=Form("hnparticlesinjet_R0%.0lf",fRpar[r]*10);
		hnparticlesinjets[r] = new TH2D(hname,"#particles in jet vs jet pT; # of particels; p_{T}^{lead} [GeV/c]", 20, 0, 20, npTleadbins, pTleadmin,pTleadmax);


   	for(Int_t pTl=0; pTl<npTlead; pTl++)
		{	
			hname=Form("hpT_pTl%i_R0%.0lf",pTl,fRpar[r]*10);
			TString hdesc=Form("jet pT for pTlead>%i ; p_{T} [GeV/c]",pTl);
			hpT_pTl[pTl][r]=new TH1D(hname, hdesc, nptbins, ptminbin, ptmaxbin);
		}
		}
	}//real data jets


}

//-----------------------------------------------------------------------------
void StJetAna::WriteHistograms(bool embedding, bool qa) {
	cout<<"Writing histograms"<<endl;

	hevents->Write();
   hevents->Write("hevts"); //for backward compatibility
	hevents_nw->Write();
	heventid->Write();
   hzvertex->Write();
   hrefmult->Write();
   hrefmult_weight->Write();
   hweight->Write();
	hz_refmult->Write();
	hday_refmult->Write();
	//hrunid_refmult->Write();
	hday_z->Write();
	hdeltaz->Write();
	hbbcrate_ntr->Write();
	hntr->Write();
	hntr_1gev->Write();
	hntr_5gev->Write();
	hntr_7gev->Write();
	hranking->Write();

	//calculate mean refmult vs day
	for(int day=1;day<251;day++)
	{
		TH1D *hday=(TH1D*) hday_refmult->ProjectionY(Form("hday_%i",day),day,day);
		TH1D *hdayz=(TH1D*) hday_z->ProjectionY(Form("hdayz_%i",day),day,day);
		float mean_refmult=hday->GetMean();	
		float mean_z=hdayz->GetMean();	
		//cout<<"day:"<<day<<" mean rm:"<<mean_refmult<<endl;
		hmeanrefmult->SetBinContent(day,mean_refmult);
		hmeanz->SetBinContent(day,mean_z);
		delete hday;
		delete hdayz;
	}
	hmeanrefmult->Write();
	hmeanz->Write();

	if(qa)//track and event QA
	{
		hpT_tr->Write();
		hpT_tr_glob->Write();
		hpT_prim_glob->Write();
		hpT_prim_over_glob->Write();
		htrNpoints->Write();
		htrNpoints_1gev->Write();
		htrNpoints_5gev->Write();
		htrNpoints_7gev->Write();
		hTOFmatch->Write();
		heta_phi_tr->Write();  
		heta_phi_tr_glob->Write();  
		heta_phi_tr1gev->Write();  
		heta_phi_tr5gev->Write();  
		heta_phi_tr7gev->Write();  
		hdca_z_tr->Write();
		hdca_tr->Write();
		hchi2_tr->Write();
		hdca_pT->Write();
	}
	for(int r=0;r<nR;r++)
	{
		if(embedding)
		{
			hjetpTembArea[r]->Write();
			for(Int_t pTlcut=0; pTlcut<npTlead; pTlcut++)
	      {	
				delta_pt_BG_sp[pTlcut][r]->Write();
			}
		}
		if(!embedding && !qa)//jets in real data
		{
			hpT_pTlead[r]->Write();
			heta_phi[r]->Write();
			hjetarea[r]->Write();
			hjetarea_cut[r]->Write();
			hjetpTarea[r]->Write();
			hjetpTcorrArea[r]->Write();
			hrho[r]->Write();
			hnparticlesinjets[r]->Write();

			//rescale hjetstructure by the number of jets
			for(int pTlead=0; pTlead<hnparticlesinjets[r]->GetNbinsY(); pTlead++)
			{
				int njets=0;
				for(int i=1; i<hnparticlesinjets[r]->GetNbinsX();i++)
				{
						njets+=hnparticlesinjets[r]->GetBinContent(i,pTlead);
				}

				
				//pTlead2=hjetstructure[r]->GetYaxis()->FindBin(hnparticlesinjets[r]->GetYaxis()->GetBinCenter(pTlead)); // this is not necessary since hjetstructure and hnparticlesinjets have the same Y binning
				for(int i=1;i<hjetstructure[r]->GetNbinsX();i++)
				{	
					float oldval=hjetstructure[r]->GetBinContent(i, pTlead);
					float newval=(njets>0) ? (oldval/njets) : 0; 
					hjetstructure[r]->SetBinContent(i,pTlead, newval);
					hjetstructure[r]->SetBinError(i,pTlead, TMath::Sqrt(newval));
						
				}
			}//pTlead cuts
			hjetstructure[r]->Write();
			

	
			for(Int_t pTl=0; pTl<npTlead; pTl++)
		   {
				hpT_pTl[pTl][r]->Write();
			}
		}//real data jets
	}//R

}

//----------------------------------------------------------------------------- 
void StJetAna::Clear(Option_t *opt) {
}

//----------------------------------------------------------------------------- 
//Main part of the analysis - using HF picoDsts
//----------------------------------------------------------------------------- 

Int_t StJetAna::Make_HFpico() {
  if(!mPicoDstMaker) {
    LOG_WARN << " No PicoDstMaker! Skip! " << endm;
    return kStWarn;
  }

  mPicoDst = mPicoDstMaker->picoDst();
  if(!mPicoDst) {
    LOG_WARN << " No PicoDst! Skip! " << endm;
    return kStWarn;
  }

  //mPicoDst->Print();
  //mPicoDst->printTracks();

//event cuts
	StPicoEvent *ev =(StPicoEvent*)mPicoDst->event();

	//select only vpd_mb_protected triggers
	if(sTrigger=="MB"){
		if(!ev->isMinBias()) return kStOK; 
	}
	else if(sTrigger=="HT0"){
		if(!ev->isHT(0)) return kStOK;
	}
	else if(sTrigger=="HT1"){
		if(!ev->isHT(1)) return kStOK;
	}
	else if(sTrigger=="HT2"){
		if(!ev->isHT(2)) return kStOK;
	}
	int runId=ev->runId();
	//cout<<"RUN_ID:"<<runId<<endl;
	int eventId=ev->eventId();
	//cout<<"event id:"<<eventId<<endl;
	int day=ev->day();
	int year=ev->year();
	StThreeVectorF vertex=(StThreeVectorF) ev->primaryVertex();
	//float bfield=ev->bField();
	float zVertex=vertex.z();//z-vertex from TPC
	float zVPD=ev->vzVpd(); //z-vertex from VPD
	float ZDCx=ev->ZDCx();
	int refMult=ev->refMult();
	int refmultCor=0;
	float bbcrate=(ev->bbcWestRate()+ev->bbcEastRate())/2.0;
	float ranking=ev->ranking();
	float weight =1.0;

	//apply event cuts (if we are not running QA)
	if(kDoEventCuts){
		if(BadDay(day,year)) return kStOK;
		//if(day!=132) return kStOK; //for comparison with Martin's results
		if(TMath::Abs(zVertex)>fZVertexCut)	return kStOK;

		if(kDoAuAu){
			if(BadRun(runId)) //we don't have parameters for refmultCorrUtil for these runs
			{
				//cout<<"bad run id: "<<runId<<endl;
				return kStOK;
			}
			refmultCorrUtil->init(runId);
			if(refmultCorrUtil->isBadRun(runId)){
				//LOG_WARN << "bad run - skip" <<endm; 
				//return kStWarn;
	  			return kStOK;
			}
			refmultCorrUtil->initEvent(refMult, zVertex, ZDCx);
			weight = refmultCorrUtil->getWeight();
			refmultCor = refmultCorrUtil->getRefMultCorr();
			//Apply centrality cuts
			if(refmultCor<=nRefmultcutMin || refmultCor>nRefmultcutMax){
				//LOG_WARN << "multiplicity out of range - skip" <<endm; 
				//return kStWarn;
	  			return kStOK;
			}
		}//AuAu
		else //pp
		{
			//cout<<"ranking:"<<ranking<<endl;
			if(TMath::Abs(zVertex-zVPD)>fDeltaZ) return kStOK;
			if(ranking<=0)return kStOK; //ranking>0 = Vertex has at least two tracks matched with the fast detector
			if(bbcrate<nBBCmin || bbcrate>nBBCmax)return kStOK;
		}
	}//apply event cuts
	else{ //apply only cuts on centrality class
		if(kDoAuAu) 
		{
			if(BadRun(runId)) //we don't have parameters for refmultCorrUtil for these runs
			{	
				if(refMult<=nRefmultcutMin || refMult>nRefmultcutMax)	return kStOK; //we don't have refmultcorr in this case so we use only refMult
			}
			else
			{
				refmultCorrUtil->init(runId);
				refmultCorrUtil->initEvent(refMult, zVertex, ZDCx);
				refmultCor = refmultCorrUtil->getRefMultCorr();
				if(refmultCor<=nRefmultcutMin || refmultCor>nRefmultcutMax) return kStOK;	
			}
			
		}//AuAu	
	}//QA

		
	vector<PseudoJet> input_vector;
	//loop over tracks
	int nTs = mPicoDst->numberOfTracks();
	int tr_all=0; //number of tracks hich pass the cuts before checking TOFvBEMC match
	int tr_match=0; //number of tracks which have a match in TOF or BEMC
	int ntracks[4]={0,0,0,0}; //number of tracks which pass all the cuts; ntracks=tr_all in AuAu and ntracks=tr_match in pp
	for(int i=0;i<nTs;i++) 
	{
   	StPicoTrack *tr = (StPicoTrack*)mPicoDst->track(i);
	   if(!tr) continue;

		StThreeVectorF pMom=(StThreeVectorF) tr->pMom();
		StThreeVectorF gMom=(StThreeVectorF) tr->gMom(/*vertex,bfield*/);
		float track_pT=pMom.perp();
		float track_pT_glob=gMom.perp();
		float track_eta=pMom.pseudoRapidity();
		float track_eta_glob=gMom.pseudoRapidity();
		float track_phi=pMom.phi();
		float track_phi_glob=gMom.phi();
		int nHitsFit=tr->nHitsFit();
		int nHitsMax=tr->nHitsMax();
		//float dca_old=TMath::Abs(tr->dca());
		float dca=TMath::Abs(tr->dca());
		float chi2;
		if(kGlobal)	chi2=TMath::Abs(tr->chi2()); //global track chi2
		else chi2=TMath::Abs(tr->chi2prob()); //primary track chi2
		//float dcaZ=TMath::Abs(tr->dcaZ());
		
		//cout<<"dca:"<<dca<<" dcaZ:"<<dcaZ<<endl;

		float nSPi=TMath::Abs(tr->nSigmaPion());
		float nSK=TMath::Abs(tr->nSigmaKaon());
		float nSP=TMath::Abs(tr->nSigmaProton());
		float fitratio=(float) nHitsFit/nHitsMax;
		//apply cuts
		if(track_pT != track_pT) continue; // that is a NaN test. It always fails if track_pT = nan.
      if(nHitsFit<nFitPointsCut)continue;
		if(fitratio<fFitOverMaxPointsCut)continue;
      if(!kGlobal && (track_pT<fpTminCut || track_pT>fpTmaxCut)) continue;
      if(kGlobal && (track_pT_glob<fpTminCut || track_pT_glob>fpTmaxCut)) continue; 
      if(dca>fDCACut)continue;
      if(nSPi>nSigmaCut && nSK>nSigmaCut && nSP>nSigmaCut)continue; //is it a charged hadron?
		if(!kGlobal && TMath::Abs(track_eta)>fMaxRap)continue;
		if(kGlobal && TMath::Abs(track_eta_glob)>fMaxRap)continue;
		if(chi2>fChi2Cut)continue;

		//if(track_pT_glob>3.0 || track_pT<7.0) continue;		

	   //remove pile-up tracks (in pp collsions) by requring a match in TOF or BEMC	
		bool TOF_match=0;
		if(!kDoAuAu)
		{
			tr_all++;
			bool inTOF=tr->btofMatchFlag();
			//if(inTOF)cout<<"match in TOF"<<endl;
			bool inBEMC=(tr->bemcId()<0) ? 0 : 1;
			//if(inBEMC) cout<<"BEMC index:"<<tr->bemcId()<<endl;
			if(inTOF || inBEMC)tr_match++;
			//if(inBEMC)tr_match++;
			else if(kreqTOF)continue;
	
				
		}
		else //calculate (TOF v  BEMC) matching efficiency in AuAu data
		{
			tr_all++;
         bool inTOF=tr->btofMatchFlag();
         //if(inTOF)cout<<"match in TOF"<<endl;
         bool inBEMC=(tr->bemcId()<0) ? 0 : 1;
         //if(inBEMC) cout<<"BEMC index:"<<tr->bemcId()<<endl;
         if(inTOF || inBEMC) 
			{
				tr_match++;
				TOF_match=1;
			}
		}
		ntracks[0]++;
		if(track_pT>1.0) ntracks[1]++;
		if(track_pT>5.0) ntracks[2]++;
		if(track_pT>7.0) ntracks[3]++;

		
//	fill QA histograms
		if(kDoQA)
		{
			if(track_phi<0)track_phi=2*TMath::Pi()+track_phi;
			if(track_phi_glob<0)track_phi_glob=2*TMath::Pi()+track_phi_glob;
			FillQAHistos(track_pT, track_eta, track_phi, track_pT_glob, track_eta_glob, track_phi_glob,dca, chi2, nHitsFit,nHitsMax,zVertex,zVPD, TOF_match, weight);

			//fill TTree
			fpT_glob=track_pT_glob;
			fpT_prim=track_pT;
			feta_glob=track_eta_glob;
			feta_prim=track_eta;
			fphi_glob=track_phi_glob;
			fphi_prim=track_phi;
			fDCAg_old=0;
			fDCAg_new=dca;
			fDCAxy=0;
			fDCAz=0;
			fchi2=chi2;
			nNfit=nHitsFit;
			nNfitNmax=nHitsMax;

			if(kSaveTree) fTrackTree->Fill();
		}

		//input vector for jet reconstruction
		PseudoJet inp_particle;
		if(kGlobal) inp_particle.reset_momentum(gMom.x(),gMom.y(),gMom.z(),gMom.mag()); //take E=/m=0/=pc=pMom.magnitude()
		else inp_particle.reset_momentum(pMom.x(),pMom.y(),pMom.z(),pMom.mag()); //take E=/m=0/=pc=pMom.magnitude()
      input_vector.push_back(inp_particle);

	}//loop over tracks
	
	//cout<<"matched tracks fraction:"<<(float)(tr_match*100)/tr_all<<"%"<<endl;
	//fill event histograms
	FillEventHistos(zVertex, zVPD, ranking, refMult, day, eventId,runId, bbcrate, ntracks, weight);

//fill event info Tree and quit
	if(kDoQA)
	{
	 //fEventTree->ResetBranchAddresses();
		fZvertex=zVertex;
		fRefMultCor=refmultCor;
		fRefMult=refMult;
		fZvpd=zVPD;
		nRunID=runId;
		if(kSaveTree) fEventTree->Fill();

		return kStOK;
	}

	//run jet reconstruction
	for(int r=0; r<nR; r++)
	{	
	float R=fRpar[r];
	//cout<<"running jet reconstruction"<<endl;
	if(!kDoEmbedding)
		JetReco(input_vector, R, nJetsRemove, weight);
	else //embedding
		for (Int_t iemb = 0; iemb < nEmb; iemb++)
		{
			JetReco(input_vector, R, nJetsRemove, weight,iemb);
		}
	}
  return kStOK;

}
//----------------------------------------------------------------------------- 
//Main part of the analysis - using STAR PYTHIA+GEANT simulation
//----------------------------------------------------------------------------- 
/*int StJetAna::Make_Pythia(int nev)
{
	StMcEvent *mcEvent =  (StMcEvent*) GetDataSet("StMcEvent");
}

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
void MyMaker::doMcTracks(){
        int nCount = 0;
        const StPtrVecMcTrack& mcTracks = mcEvent->primaryVertex()->daughters();
        StMcTrackConstIterator mcTrkIter = mcTracks.begin();
        for ( ; mcTrkIter != mcTracks.end(); ++mcTrkIter) {
                StMcTrack* track = *mcTrkIter;
                TStarJetPicoPrimaryTrack mTrack;
                // cout << track->parent() << endl;
                // if( track->parent() != 0) continue;
                // cout << "Saving track " << endl;
                mTrack.SetPx(static_cast<float>(track->momentum().x()));
                mTrack.SetPy(static_cast<float>(track->momentum().y()));
                mTrack.SetPz(static_cast<float>(track->momentum().z()));
                mTrack.SetDCA(0);
                //   mTrack.SetdEdx(static_cast<float>(track->pdgId()));
                mTrack.SetNsigmaPion(0);
                mTrack.SetNsigmaKaon(0);
                mTrack.SetNsigmaProton(0);
                mTrack.SetNsigmaElectron(0);
                if( track->particleDefinition()){
						mTrack.SetCharge(static_cast<int>(track->particleDefinition()->charge()));
						mTrack.SetdEdx(static_cast<int>(track->particleDefinition()->pdgEncoding()));
                }
                else{
                        if(mVerbose) cout << "Particle with no encoding " << endl;
                        mTrack.SetCharge(0);
                        mTrack.SetdEdx(0);
                }
                //mTrack.SetNOfFittedHits(10);
               mTrack.SetNOfFittedHits(static_cast<int>(track->tpcHits().size()));
                mTrack.SetNOfPossHits(52);
                mTrack.SetKey(static_cast<int>(track->key()));
                mTrack.SetEtaDiffHitProjected(0);
                mTrack.SetPhiDiffHitProjected(0);
                nCount++;
                //cout<<mTrack.GetPx()<<"  "<<mTrack.GetPy()<<" "<<mTrack.GetPz()<<"  "<<track->geantId()<<" "<<track->pdgId()<<endl;
                MyMcEvent->AddPrimaryTrack(&mTrack);
        }
        MyMcEvent->GetHeader()->SetEventId(static_cast<int>(mcEvent->eventNumber()));
        MyMcEvent->GetHeader()->SetRunId(static_cast<int>(mcEvent->runNumber()));
        MyMcEvent->GetHeader()->SetReferenceMultiplicity(static_cast<int>(mcEvent->eventGeneratorFinalStateTracks()));
        MyMcEvent->GetHeader()->SetNPrimaryTracks(static_cast<int>(nCount));
        MyMcEvent->GetHeader()->SetPrimaryVertexX(static_cast<float>(mcEvent->primaryVertex()->position().x()));
        MyMcEvent->GetHeader()->SetPrimaryVertexY(static_cast<float>(mcEvent->primaryVertex()->position().y()));
        MyMcEvent->GetHeader()->SetPrimaryVertexZ(static_cast<float>(mcEvent->primaryVertex()->position().z()));
        MyMcEvent->GetHeader()->SetNOfPrimaryTracks(nCount);
        return;
}

*/
//----------------------------------------------------------------------------- 
//Main part of the analysis - using Jan's picoDsts
//----------------------------------------------------------------------------- 
/*int StJetAna::Make_Janpico(int nev)
{


	TStarJetPicoReader readerdata;
  	TStarJetPicoEventCuts* evCuts = readerdata.GetEventCuts();

  evCuts->SetTriggerSelection("MB"); //All, MB, HT, pp, ppHT, ppJP

  evCuts->SetVertexZCut(fZVertexCut);//[cm] 
  //evCuts->SetRefMultCut(refmultcut);

  readerdata.GetTrackCuts()->SetDCACut(fDCACut);
  readerdata.GetTrackCuts()->SetMinNFitPointsCut(nFitPointsCut);
  readerdata.GetTrackCuts()->SetFitOverMaxPointsCut(fFitOverMaxPointsCut);
  readerdata.SetApplyMIPCorrection(kFALSE);
  readerdata.SetFractionHadronicCorrection(1.0); //0-1 - what fraction of charged track pT will be subtracted from deposited tower energy


	TChain *ch = TStarJetPicoUtils::BuildChainFromFileList(sInFileList.Data(),"JetTree",1000,0);
	readerdata.SetInputChain(ch);
	readerdata.Init(nev);
	Int_t evt = 0;

	while (readerdata.NextEvent() == kTRUE)
   {
      TStarJetPicoEvent *event = readerdata.GetEvent();
      TStarJetPicoEventHeader *header = event->GetHeader();
      int runId = header->GetRunId();
      float refMult = header->GetReferenceMultiplicity();
      float zVertex = header->GetPrimaryVertexZ();
      float ZDCx = header->GetZdcCoincidenceRate();

		if(kDoAuAu)
		{
			if(BadRun(runId)) 
			{
				//cout<<"bad run id: "<<runId<<endl;
				continue;
			}

			refmultCorrUtil->init(runId);
			if(refmultCorrUtil->isBadRun(runId)){
				//LOG_WARN << "bad run - skip" <<endm; 
				//return kStWarn;
				continue;
			}
		}

		if(TMath::Abs(zVertex)>fZVertexCut){
			//LOG_WARN << "z-vertex cut not passed - skip" <<endm; 
	   	//return kStWarn;
			continue;
		}

		float weight =1.0;
		if(kDoAuAu){
			refmultCorrUtil->initEvent(refMult, zVertex, ZDCx);
			weight = refmultCorrUtil->getWeight();
			int refmultCor = refmultCorrUtil->getRefMultCorr();
			if(refmultCor<=nRefmultcutMin || refmultCor>nRefmultcutMax){
				//LOG_WARN << "multiplicity out of range - skip" <<endm; 
				//return kStWarn;
	  			continue;
			}
		}

		// ***************
		// track loop
		// ***************
		vector<PseudoJet> input_vector;
		TList*  primTracks          = readerdata.GetListOfSelectedTracks();
      TIter nextTrack(primTracks);
      TStarJetPicoPrimaryTrack* primTrack;
      while(primTrack = (TStarJetPicoPrimaryTrack*)nextTrack()  )
      {
	      float track_px      = primTrack->GetPx();
	      float track_py      = primTrack->GetPy();
	      float track_pz      = primTrack->GetPz();
	      float track_pT      = TMath::Sqrt(track_px*track_px+track_py*track_py);
         float track_eta     = primTrack->GetEta();
         float track_phi     = primTrack->GetPhi();
         float dca  = primTrack->GetsDCAxy();
         int nHitsFit     = primTrack->GetNOfFittedHits();
			float nSPi = primTrack->GetNsigmaPion();
			float nSK = primTrack->GetNsigmaKaon();
			float nSP = primTrack->GetNsigmaProton();

		//apply cuts
		if(track_pT != track_pT) continue; // that is a NaN test. It always fails if track_pT = nan.
      if(nHitsFit<nFitPointsCut)continue;
		//if(fitratio<fFitOverMaxPointsCut)continue;
      if(track_pT<fpTminCut || track_pT>fpTmaxCut) continue;
      if(TMath::Abs(dca)>fDCACut)continue;
      if(nSPi>nSigmaCut && nSK>nSigmaCut && nSP>nSigmaCut)continue; //is it a charged hadron?
		if(TMath::Abs(track_eta)>fMaxRap)continue;

		//	fill QA histograms
		if(kDoQA)
		{
			FillQAHistos(track_pT, track_eta, track_phi, dca, zVertex, weight);
		}

		//input vector for jet reconstruction
		PseudoJet inp_particle(track_px,track_py,track_pz,TMath::Sqrt(track_px*track_px+track_py*track_py+track_pz*track_pz));
      input_vector.push_back(inp_particle);

	}//loop over tracks
	
	//fill event histograms
	FillEventHistos(zVertex, refMult, day, weight);

	if(kDoQA){continue;}

	// ***************************
	//run jet reconstruction
	// ***************************
	for(int r=0; r<nR; r++)
	{	
	float R=fRpar[r];
	//cout<<"running jet reconstruction"<<endl;
	if(!kDoEmbedding)
		JetReco(input_vector, R, nJetsRemove, weight);
	else //embedding
		for (Int_t iemb = 0; iemb < nEmb; iemb++)
		{
			JetReco(input_vector, R, nJetsRemove, weight,iemb);
		}
	}

	}//event loop
  return kStOK;

}*/


//----------------------------------------------------------------------------- 
//Main part of the analysis - using Alex's picoDsts - NOT USED ANYMORE
//----------------------------------------------------------------------------- 

int StJetAna::Make_Alexpico(int nev)
{
/*
	Int_t start_event_use=0;
	Int_t stop_event_use=nev+start_event_use;

	TChain *ch;
	StJetTrackEvent     *JetTrackEvent;
	StJetTrackParticle  *JetTrackParticle;

	//-----------------------------------------------------
	//Load Input Files
	//-----------------------------------------------------
	Long64_t entries_save = 0;
	Int_t iFile=0;
	if (!sInFileList.IsNull())   // if input file is ok
	{
		cout << "Open file list " << sInFileList << endl;
		ifstream in(sInFileList);  // input stream
		if(in)
		{		
			cout << "file list is ok" << endl;
			ch = new TChain( ALEX_EVENT_TREE, ALEX_EVENT_TREE );
			char strg[255];       // char array for each file name
			while(in)
			{
				in.getline(strg,255);  // take the lines of the file list
				if(strg[0] != 0)
				{
					iFile++;
					//if(iFile<=nSkipFiles)continue;
					//if(iFile>nSkipFiles+nFiles)continue;
					TString addfile;
					addfile += strg;
					addfile = sInputdir+addfile;
					Long64_t file_entries;
					ch->AddFile(addfile.Data(),-1, ALEX_EVENT_TREE );
					file_entries = ch->GetEntries();
					cout << "File added to data chain: " << addfile.Data() << " with " << (file_entries-entries_save) << " entries" << endl;
					entries_save = file_entries;
				}
			}
		}
	}
	else
	{
		cout << "WARNING: file input is problemtic" << endl;
	}

	JetTrackEvent = new StJetTrackEvent();
	ch->SetBranchAddress( ALEX_EVENT_BRANCH, &JetTrackEvent );


	//---------------------------
	//EVENT LOOP
	//---------------------------

	int evt=0;
	Long64_t stop_event_use_loop = stop_event_use;
	if(stop_event_use_loop > entries_save) stop_event_use_loop = entries_save;
		for(Long64_t counter = start_event_use; counter < stop_event_use_loop; counter++)
		{
			if (counter != 0  &&  counter % 100 == 0)
				cout << "." << flush;
			if (counter != 0  &&  counter % 1000 == 0)
			{
				if((stop_event_use_loop-start_event_use) > 0)
				{
					Double_t event_percent = 100.0*((Double_t)(counter-start_event_use))/((Double_t)(stop_event_use_loop-start_event_use));
					cout << " " << counter << " (" << event_percent << "%) " << "\n" << "==> Processing data " << flush;
				}
			}

	if (!ch->GetEntry( counter )) // take the event -> information is stored in event
	break;


	//Float_t  prim_vertex_x   = JetTrackEvent->getx();
	//Float_t  prim_vertex_y   = JetTrackEvent->gety();
	Float_t  zVertex   = JetTrackEvent->getz();
	Int_t    runId           = JetTrackEvent->getid();
	Int_t day=(runId%1000000)/1000;
	Float_t  refMult         = JetTrackEvent->getmult();
	Float_t  ZDCx            = JetTrackEvent->getZDCx();
	//Float_t  BBCx            = JetTrackEvent->getBBCx();
	Float_t  vzVPD           = JetTrackEvent->getvzVpd();
	Int_t    N_Particles     = JetTrackEvent->getNumParticle();
	
	if(kDoAuAu)
	{
		if(BadRun(runId)) 
		{
			//cout<<"bad run id: "<<runId<<endl;
			continue;
		}

		refmultCorrUtil->init(runId);
		if(refmultCorrUtil->isBadRun(runId)){
			//LOG_WARN << "bad run - skip" <<endm; 
			//return kStWarn;
			continue;
		}
	}

	if(TMath::Abs(zVertex)>fZVertexCut){
		//LOG_WARN << "z-vertex cut not passed - skip" <<endm; 
   	//return kStWarn;
		continue;
	}

	float weight =1.0;
	if(kDoAuAu){
		refmultCorrUtil->initEvent(refMult, zVertex, ZDCx);
		weight = refmultCorrUtil->getWeight();
		int refmultCor = refmultCorrUtil->getRefMultCorr();
		if(refmultCor<=nRefmultcutMin || refmultCor>nRefmultcutMax){
			//LOG_WARN << "multiplicity out of range - skip" <<endm; 
			//return kStWarn;
  			continue;
		}
	}

	// ***************
	//track loop
	// ***************
	vector<PseudoJet> input_vector;

	int ntracks[4]={0,0,0,0};
   for(Int_t i_Particle = 0; i_Particle < N_Particles; i_Particle++)
   {
                // Particle information
                JetTrackParticle            = JetTrackEvent->getParticle(i_Particle);
                Float_t dca                 = JetTrackParticle->get_dca_to_prim();
                //Float_t m2                  = JetTrackParticle->get_Particle_m2 ();
                Float_t nSPi                = TMath::Abs(JetTrackParticle->get_Particle_nSigmaPi());
                Float_t nSK                 = TMath::Abs(JetTrackParticle->get_Particle_nSigmaK());
                Float_t nSP                 = TMath::Abs(JetTrackParticle->get_Particle_nSigmaP());
                //Float_t qp                  = JetTrackParticle->get_Particle_qp();
                Float_t nHitsFit            = JetTrackParticle->get_Particle_hits_fit();
                TLorentzVector TLV_Particle_prim = JetTrackParticle->get_TLV_Particle_prim();
                TLorentzVector TLV_Particle_glob = JetTrackParticle->get_TLV_Particle_glob();

                TLorentzVector TLV_Particle_use = TLV_Particle_prim;
                if(kGlobal) TLV_Particle_use = TLV_Particle_glob;

                Double_t track_phi    = TLV_Particle_prim.Phi();
                Double_t track_pT     = TLV_Particle_prim.Pt();

                Double_t track_phi_glob    = TLV_Particle_glob.Phi();
                Double_t track_pT_glob     = TLV_Particle_glob.Pt();

		//apply cuts
		if(track_pT != track_pT) continue; // that is a NaN test. It always fails if track_pT = nan.
      if(nHitsFit<nFitPointsCut)continue;
		//if(fitratio<fFitOverMaxPointsCut)continue;
      if(!kGlobal && (track_pT<fpTminCut || track_pT>fpTmaxCut)) continue;
      if(kGlobal && (track_pT_glob<fpTminCut || track_pT_glob>fpTmaxCut)) continue;
      if(TMath::Abs(dca)>fDCACut)continue;
      if(nSPi>nSigmaCut && nSK>nSigmaCut && nSP>nSigmaCut)continue; //is it a charged hadron?

					 Double_t track_eta    = TLV_Particle_prim.PseudoRapidity();
					 Double_t track_eta_glob    = TLV_Particle_glob.PseudoRapidity();

		if(!kGlobal && TMath::Abs(track_eta)>fMaxRap)continue;
		if(kGlobal && TMath::Abs(track_eta_glob)>fMaxRap)continue;

		ntracks[0]++;
      if(track_pT>1.0) ntracks[1]++;
      if(track_pT>5.0) ntracks[2]++;
      if(track_pT>7.0) ntracks[3]++;

//	fill QA histograms
		if(kDoQA)
		{
			FillQAHistos(track_pT, track_eta, track_phi, track_pT_glob, track_eta_glob, track_phi_glob,dca, -1, nHitsFit,-1,zVertex,vzVPD, 0, weight);
		}

		//input vector for jet reconstruction
		PseudoJet inp_particle(TLV_Particle_use.Px(),TLV_Particle_use.Py(),TLV_Particle_use.Pz(),TLV_Particle_use.E());
      input_vector.push_back(inp_particle);

	}//loop over tracks
	
	//fill event histograms
	FillEventHistos(zVertex, vzVPD, -1, refMult, day, -1 ,runId, -1, ntracks, weight);

	if(kDoQA){continue;}

	// ***************************
	// run jet reconstruction
	// ***************************
	for(int r=0; r<nR; r++)
	{	
	float R=fRpar[r];
	//cout<<"running jet reconstruction"<<endl;
	if(!kDoEmbedding)
		JetReco(input_vector, R, nJetsRemove, weight);
	else //embedding
		for (Int_t iemb = 0; iemb < nEmb; iemb++)
		{
			JetReco(input_vector, R, nJetsRemove, weight,iemb);
		}
	}

	}//event loop
  return kStOK;
*/
}

//----------------------------------------------------------------------------- 
//jet reconstruction with fast jet 3
//----------------------------------------------------------------------------- 
void StJetAna::JetReco(vector<PseudoJet> input_vector_data, float R, short Remove_N_hardest, float weight, short embedding)
{

	//declare variables
	vector<PseudoJet> input_vector;
	float pT_emb=0;
	float eta_emb=0;
	float phi_emb=0;
	float maxRapJet=fMaxRap - R; //fiducial jet acceptance
	//float maxRapJet=fMaxRap - 0.3; //fiducial jet acceptance

	//embedding of simulated jet into real event
	if(kDoEmbedding)
	{	
		pT_emb=fEmbPt[embedding];
		input_vector=EmbedJet(kPythiaEmb,pT_emb,R,input_vector_data/*,&eta_emb,&phi_emb*/,sParton,fpythia);
	}
	else input_vector=input_vector_data;

	//setup fastjet
	JetDefinition jet_def(antikt_algorithm, R);
	// jet area definition
	//GhostedAreaSpec area_spec(fGhost_maxrap);
	//AreaDefinition area_def(active_area, area_spec);
	AreaDefinition area_def(active_area_explicit_ghosts,GhostedAreaSpec(fGhost_maxrap,1,0.01));

	//run jet reconstruction
	ClusterSequenceArea clust_seq_hard(input_vector, jet_def, area_def);
	vector<PseudoJet> jets_all = sorted_by_pt(clust_seq_hard.inclusive_jets(fpTminCut));
	Selector Fiducial_cut_selector = SelectorAbsEtaMax(maxRapJet); // Fiducial cut for jets
	vector<PseudoJet> jets = Fiducial_cut_selector(jets_all);

	// background estimation
   JetDefinition jet_def_bkgd(kt_algorithm, fR_bg); 
   AreaDefinition area_def_bkgd(active_area_explicit_ghosts,GhostedAreaSpec(fGhost_maxrap,1,0.01));
   Selector selector = SelectorAbsEtaMax(1.0) * (!SelectorNHardest(Remove_N_hardest)); 
   JetMedianBackgroundEstimator bkgd_estimator(selector, jet_def_bkgd, area_def_bkgd); 
   bkgd_estimator.set_particles(input_vector);

   float rho   = bkgd_estimator.rho();
   float rho_sigma = bkgd_estimator.sigma();
	
	//calculate ridx = position of R in fRpar array
	int ridx=0;
	for(int r=0; r<nR; r++){
		if(TMath::Abs(R-fRpar[r])<0.001)	ridx=r;
	}

	if(!kDoEmbedding) hrho[ridx]->Fill(rho, weight);

	//loop over jets
	for(Int_t pjet=0; pjet<jets.size(); pjet++)
	{
      Double_t phi_jet = jets[pjet].phi();
      Double_t eta_jet = jets[pjet].eta();
      Double_t pT_jet = jets[pjet].perp();
      Double_t area_jet = jets[pjet].area();
      vector<PseudoJet> constituents = sorted_by_pt(jets[pjet].constituents());
      Double_t pTlead = constituents[0].perp();
		Double_t pTcorr_jet = pT_jet - area_jet*rho;

		//set acceptance
      Float_t etaMinCut=-(maxRapJet);
      Float_t etaMaxCut=(maxRapJet);
		
      if(eta_jet<etaMinCut || eta_jet>etaMaxCut) continue; // fiducial acceptance 

		//*********************
      //FILLING HISTOGRAMS
		//*********************
		bool found=false;
		if(kDoEmbedding)
		{
			//is it embedded jet?
			found=FindEmbeddedJet(constituents,pT_emb);
			if(found) FillJetHistosEmb(pTcorr_jet, area_jet, pTlead,pT_emb, ridx, weight/*,eta_jet,phi_jet, eta_emb,phi_emb*/);
		}
		else
		{
			short nparticles=constituents.size();
			FillJetHistos(pT_jet, pTcorr_jet, area_jet, pTlead, eta_jet, phi_jet, ridx,nparticles, weight);
			
			//loop over jet constituents
			for(int pr=0; pr<nparticles; pr++)
			{
				//apply jet area cut
				if(area_jet < fAcuts[ridx]) continue;
				float part_pT=constituents[pr].perp();
				hjetstructure[ridx]->Fill(part_pT,pTlead);
			}
			

		}
	}//jet loop
	return;	
}

//----------------------------------------------------------------------------- 
//Generate jet for embedding
//jetType: 0...single particle jet | 1...Pythia jet (TBD)
//----------------------------------------------------------------------------- 
vector<PseudoJet> StJetAna::EmbedJet(short jetType, float &pT_emb, float R, vector<PseudoJet> container/*, float* eta_emb, float* phi_emb*/, TString parton, TPythia6* pythia)
{
	float maxRapJet=fMaxRap - R; //fiducial jet acceptance
	//float maxRapJet=fMaxRap - 0.3; //fiducial jet acceptance
	double eta_rnd = gRandom->Uniform(-(maxRapJet),(maxRapJet));
   double phi_rnd = gRandom->Uniform(0, 2.*TMath::Pi());
/*	
	*eta_emb=eta_rnd;
	*phi_emb=phi_rnd;
*/	

	if(jetType==0) //single-particel jet
	{	
		TLorentzVector v;
	   v.SetPtEtaPhiM(pT_emb, eta_rnd, phi_rnd, 0);
   	PseudoJet embeddedParticle=PseudoJet(v.Px(), v.Py(), v.Pz(), v.E());
	   embeddedParticle.set_user_index(99999);
		container.push_back(embeddedParticle);
	}
	else if(jetType==1) //pythia jet
	{
//cout<<"Memory used1: "<<memstat.Used()<<endl;
	
		bool charged=0; //we embed full jets since we need exact jet pT
  		TClonesArray *simarr = new TClonesArray("TParticle", 1000);
	
		//bool goodjet=0;
  		//while(!goodjet)
		//{
     	//simarr->Delete();
      Double_t phi_parton = phi_rnd;
      Double_t eta_parton = eta_rnd;
      Double_t theta = 2.0*TMath::ATan(TMath::Exp(-1*eta_parton));
      Double_t E = (1.03*pT_emb+0.5)/TMath::Sin(theta); //most of the jets (after constituent cuts) will have only 95% of the required pTemb => *1/0.95 = 1.05
      
		if(sParton=="u")
   	   pythia->Py1ent(0, 2, E, theta, phi_parton); //u->jet
		else if (sParton=="g")
	      pythia->Py1ent(0, 21, E, theta, phi_parton); //g->jet

      pythia->Pyexec();

      TLorentzVector partonlv(0, 0, 0, 0);
      partonlv.SetPtEtaPhiE(pT_emb, eta_parton, phi_parton, E);

      Int_t final = pythia->ImportParticles(simarr, "Final");
      Int_t nparticles = simarr->GetEntries();
     //cout<<"npart: "<<nparticles<<endl; 
      //TLorentzVector conelvPart; //particle level jet vector
  		TLorentzVector partlv; //particle level jet constituent vector

      Int_t goodparpart = 0;
		TLorentzVector foundlv(0., 0., 0., 0.);
      for(Int_t ipart = 0; ipart < nparticles; ipart++)
		{
			TParticle *particle = (TParticle*)simarr->At(ipart);

	  
	  		particle->Momentum(partlv);
	 		//cout<<ipart<<"| eta:"<<partlv.Eta()<<" phi: "<<partlv.Phi()<<endl; 
	  		partlv.SetPtEtaPhiM(partlv.Pt(), partlv.Eta(), partlv.Phi(), 0); //set M=0, probably not necessary

	  		Double_t pTpart = partlv.Pt();
    
 	  		if(pTpart < 0.2) continue; //undetectable soft particles
	 
	 		if(charged)
         {
          	Double_t charge = particle->GetPDG()->Charge();
          	if(!charge) continue;
         }

	  		Double_t eta = partlv.Eta();
		  	Double_t phi = partlv.Phi();
			Double_t M = 0;

	  		//conelvPart += partlv;
			PseudoJet embeddedParticle=PseudoJet(partlv.Px(), partlv.Py(), partlv.Pz(), partlv.E());
	   	embeddedParticle.set_user_index(99999);
			container.push_back(embeddedParticle);
			if(TMath::Abs(eta<1)) foundlv += partlv;

		}//particle loop [in particle level jet]
     	simarr->Delete();
		delete simarr;

		pT_emb=foundlv.Pt(); //update pT_emb value

	}//pythia jet
	return container;
}
//----------------------------------------------------------------------------- 
//Find embedded jet
//----------------------------------------------------------------------------- 
bool StJetAna::FindEmbeddedJet(vector<PseudoJet> constituents,float pT_emb)
{

		TLorentzVector foundlv(0., 0., 0., 0.);
      TLorentzVector constlv(0., 0., 0., 0.);
		int N=constituents.size();
      for(int iConst = 0; iConst < N; iConst++)
	   if(constituents[iConst].user_index() >= 90000) //embedded particles
      {
			float pT = constituents[iConst].perp();
			float eta = constituents[iConst].eta();
			float phi = constituents[iConst].phi();
			if(phi<0)phi=phi+2*TMath::Pi();
			float M = constituents[iConst].m();
			constlv.SetPtEtaPhiM(pT, eta, phi, M);
			foundlv += constlv;
		}

		if(foundlv.Pt() / pT_emb > 0.95) return true; //the jet is matched to the embedded one if it contains embedded particles which carry at least 90% energy of the embedded jet
		else return false;
}

//----------------------------------------------------------------------------- 
//filling event histograms
//----------------------------------------------------------------------------- 
void StJetAna::FillEventHistos(float zvertex, float zVPD, float ranking, int refmult, int day, int eventid, int runid, float bbcrate, int *ntracks, float weight)
{
	hevents->Fill(1,weight);
	hevents_nw->Fill(1);
	heventid->Fill(eventid);
	hzvertex->Fill(zvertex,weight);
	hrefmult->Fill(refmult,weight);
	hrefmult_weight->Fill(refmult,weight);
	hweight->Fill(weight);
	hz_refmult->Fill(zvertex,refmult,weight);   
	hday_refmult->Fill(day,refmult,weight);   
	//hrunid_refmult->Fill(runid,refmult,weight);   
	hday_z->Fill(day,zvertex,weight);   
	hdeltaz->Fill(zvertex-zVPD);
	hbbcrate_ntr->Fill(bbcrate,ntracks[0],weight);
	hntr->Fill(ntracks[0],weight);
	hntr_1gev->Fill(ntracks[1],weight);
	hntr_5gev->Fill(ntracks[2],weight);
	hntr_7gev->Fill(ntracks[3],weight);
	hranking->Fill(ranking/1000,weight);
	
	
	return;
} 

//----------------------------------------------------------------------------- 
//filling QA histograms
//----------------------------------------------------------------------------- 
void StJetAna::FillQAHistos(float track_pT, float track_eta, float track_phi, float track_pT_glob, float track_eta_glob, float track_phi_glob, float dca, float chi2, int npoints,int npointsMax, float zVertex, float zVPD, bool TOF_match, float weight)
{
	hpT_tr->Fill(track_pT, weight);
	hpT_tr_glob->Fill(track_pT_glob, weight);
	hpT_prim_glob->Fill(track_pT,track_pT_glob, weight);
	if(track_pT_glob>0) hpT_prim_over_glob->Fill(track_pT/track_pT_glob, weight);
	htrNpoints->Fill(npoints,npointsMax,weight);
	heta_phi_tr->Fill(track_eta,track_phi, weight);	
	heta_phi_tr_glob->Fill(track_eta_glob,track_phi_glob, weight);	
	
	if(track_pT>1.0)
	{
		heta_phi_tr1gev->Fill(track_eta,track_phi, weight);	
		htrNpoints_1gev->Fill(npoints,npointsMax,weight);
	}
	if(track_pT>5.0)
	{
		heta_phi_tr5gev->Fill(track_eta,track_phi, weight);	
		htrNpoints_5gev->Fill(npoints,npointsMax,weight);
	}
	if(track_pT>7.0)
	{
		heta_phi_tr7gev->Fill(track_eta,track_phi, weight);	
		htrNpoints_7gev->Fill(npoints,npointsMax,weight);
	}
	hdca_z_tr->Fill(dca, zVertex, weight);
	hdca_pT->Fill(dca, track_pT, weight);
	hdca_tr->Fill(dca, weight);
	hchi2_tr->Fill(chi2, weight);
	if(TOF_match)hTOFmatch->Fill(track_pT,weight);
	return;
}
//----------------------------------------------------------------------------- 
//filling jet histograms
//----------------------------------------------------------------------------- 
void StJetAna::FillJetHistos(float pT_jet, float pTcorr_jet, float area_jet, float pTlead, float eta_jet, float phi_jet, int r, int nparticles, float weight)
{

		hjetarea[r]->Fill(area_jet,weight);
      hjetpTarea[r]->Fill(pT_jet,area_jet,weight);
      hjetpTcorrArea[r]->Fill(pTcorr_jet,area_jet,weight);

		//apply jet area cut
		if(area_jet < fAcuts[r]) return;
		hjetarea_cut[r]->Fill(area_jet,weight);
		hpT_pTlead[r]->Fill(pTcorr_jet,pTlead,weight);
      heta_phi[r]->Fill(eta_jet,phi_jet,weight);
		hnparticlesinjets[r]->Fill(nparticles, pTlead);

      for(Int_t pTl=0; pTl<npTlead; pTl++)
		{
			if(pTlead>pTl)
			hpT_pTl[pTl][r]->Fill(pTcorr_jet,weight);
      }//loop over pTlead cuts

	return;
}

//----------------------------------------------------------------------------- 
//filling jet histograms - embedding
//----------------------------------------------------------------------------- 
void StJetAna::FillJetHistosEmb(float pTcorr_jet, float area_jet, float pTlead, float pT_emb, int r, float weight/*,float eta_jet, float phi_jet, float eta_emb,float phi_emb*/)
{	
		hjetpTembArea[r]->Fill(pT_emb,area_jet,weight);
     	if(area_jet<fAcuts[r])return;
		double dpT=	pTcorr_jet-pT_emb;
      for(Int_t pTl=0; pTl<npTlead; pTl++)
		{
			if(pTlead<pTl) continue;
			delta_pt_BG_sp[pTl][r]->Fill(pT_emb,dpT,weight);
		}
	return;
}

//----------------------------------------------------------------------------- 
//Event selection functions
//----------------------------------------------------------------------------- 
bool StJetAna::BadRun(int id)
{
	const int nbadruns=5;
	int badruns[]={12146006,12146007,12153004,12153007, 12153002};
	for(int i=0; i<nbadruns; i++)
	{
		if(id==badruns[i]) return true;
	}
	return false;
}
//----------------------------------------------------------------------------- 
bool StJetAna::BadDay(int day,short year)
{
	const int nbaddays11=10;
	const int nbaddays12=2;
	int baddays_11[]={138,139,140,141,142,143,144,145,149,150};
	int baddays_12[]={-1,-1};
	if(year==2011)
		for(int i=0; i<nbaddays11; i++)
		{
			if(day==baddays_11[i]) return true;
		}
	else if(year==2012)
		for(int i=0; i<nbaddays12; i++)
		{
			if(day==baddays_12[i]) return true;
		}
	return false;
}


