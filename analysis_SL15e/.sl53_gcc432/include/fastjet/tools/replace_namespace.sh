#!/bin/bash
#FINDSTRING="FASTJET_END_NAMESPACE"
FINDSTRING="FASTJET_BEGIN_NAMESPACE"
#REPLACESTRING="} \/\/ fastjet namespace"
REPLACESTRING="namespace fastjet {"
     for fl in *.hh; do
     mv $fl $fl.old
     sed "s/${FINDSTRING}/${REPLACESTRING}/g" $fl.old > $fl
     rm -f $fl.old
done
