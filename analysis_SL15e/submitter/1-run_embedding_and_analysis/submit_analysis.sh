#!/bin/bash
source set_paths.sh
export TRIGGER="MB" #trigger, use "MB" for AuAu data and "MB" or "HT0","HT1","HT2" for pp data
export DOAUAU=1 #AuAu or pp collisions
export DOQA=0 #do QA of tracks and events
	export SAVETREE=0 # when doing QA, save also TTree with event variables (refmult, z,...) - WARNING: output file can be large!
export DOEMBEDDING=0 #perform jet embedding in order to calculate delta-pT
	export PYTHIAEMB=0 #embed pythia jets instead of single particle
	export PARTON="g" #when embedding pythia jets, do we want to fragment u quark ("u") or gluon ("g")
export DOEVENTCUTS=1 #apply event cuts
export GLOBAL=0 #use global or primary tracks
export RRHO=0.3 #0.3 | size of jets used to calculate background energy density
export ZVERTEX=30 #30 |  maximum value of z vertex position (TPC)
export ZTPCZVPD=4.0 #maximum distance between VPD and TPC z vertex position (used only for pp)
export TOFBEMC=0 #require for each track a match in BEMC or TOF in pp collisions to remove pile-up
export BBCMIN=0
export BBCMAX=1000000000
export MAXRAP=1.0 #1.0 | max PSEUDOrapidity acceptance for tracks; (NB: maximum PSEUDOrapidity acceptance for jets is |eta|<MAXRAP-R)
export DCA=1.0 #1.0 | track maximum Distance of Closest Approach
export CHI2=100.0 #100 | track fit minimal chi2 value
export NFIT=20 # 15 | minimal number of TPC fit points 
export NFITNMAX=0.52 #0.52 |  number of TPC fit points / # possible points
export ALEXPICO=0 #use Alex's picoDst instead of Heavy Flavor group picoDst; NOT USED ANYMORE (these were on PDSF)
export NEVENTS=100000 #how many events per job do we want to analyze

SUFF="_CodeQA" #output directory suffix
#SUFF="_cutsSameAsEmb_OKnevents_R05_PythiaEmb4g" #output directory suffix

TIMESTAMP=$(date +%s)
for CENTRAL in 1 #0 # 1 #use only one value for pp data (it doesn't matter which one)
do
export CENTRAL

if [ $CENTRAL -eq 1 ]; then
     CENTRALITY="0-10"
	  REFMULTMIN=396 #drop events with refmult  less or equal REFMULTMIN
	  #REFMULTMIN=1
	  REFMULTMAX=100000 #drop events with refmult greater than REFMULTMAX
	  NJETSREMOVE=2 #2

elif [ $CENTRAL -eq 0 ]; then
      #CENTRALITY="60-80" 
      CENTRALITY="60-80" 
		REFMULTMIN=10 #80%: 10; 55%: 31
		REFMULTMAX=43 # 60-80%: 43 
		NJETSREMOVE=1 #1
elif [ $CENTRAL -eq -1 ]; then
      #CENTRALITY="60-80" 
      CENTRALITY="60-65" 
		REFMULTMIN=31 #80%: 10; 55%: 31
		REFMULTMAX=43 # 60-80%: 43 
		NJETSREMOVE=1 #1
elif [ $CENTRAL -eq -2 ]; then
      #CENTRALITY="60-80" 
      CENTRALITY="50-60" 
		REFMULTMIN=43 #80%: 10; 55%: 31
		REFMULTMAX=76 # 50-80%: 76
		NJETSREMOVE=1 #1
fi 
export REFMULTMIN
export REFMULTMAX

COLSYS="AuAu"

IN_FILELIST="$STARJETBASEDIR/filelists/run11_AuAu200_HFpicoDst_day128.list"
if [ $DOEMBEDDING -eq 1 ]; then 
	IN_FILELIST="$STARJETBASEDIR/filelists/run11_AuAu200_HFpicoDst_4embedding.list"
fi
#IN_FILELIST="$STARJETBASEDIR/filelists/test.list"
PICOTYPE="HFpico"

if [ $DOAUAU -eq 0 ]; then #pp data
	IN_FILELIST="$STARJETBASEDIR/filelists/run12_pp200_HFpicoDst_full.list"
	COLSYS="pp"
	NJETSREMOVE=1
fi
#if [ $ALEXPICO -eq 1 ]; then #not used anymore
#	IN_FILELIST="$STARJETBASEDIR/filelists/run11_AuAu200_AlexpicoDst_cent${CENTRALITY}.list"
#	PICOTYPE="ALEXpico"
#fi
export NJETSREMOVE
#export IN_FILELIST

OUTDIR="$STARJETBASEDIR/out/$TRIGGER/"
BASEDIR=$ANALYSISDIR
LOGDIR="$BASEDIR/submitter/log"
ERRDIR="$BASEDIR/submitter/err"
export MACRODIR=$BASEDIR
TEMPLATE_NAME="JetAna_${CENTRAL}_${TIMESTAMP}.xml"

BASENAME="incl"
if [ $DOEMBEDDING -eq 1 ]; then
	BASENAME="emb"
fi
if [ $DOQA -eq 1 ]; then
	BASENAME="qa"
fi

TYPE="${BASENAME}_${COLSYS}_${CENTRALITY}cent_${PICOTYPE}_SL15_evtcts${DOEVENTCUTS}_rhoR${RRHO}_nrem${NJETSREMOVE}_nFitnMax${NFITNMAX}_NFIT${NFIT}_dca${DCA}_chi2-${CHI2}_glob${GLOBAL}$SUFF"


NFILES=200 #how many files merge into one batch
if [ $CENTRAL -eq 1 ]; then
	NFILES=100
fi
if [ $DOEMBEDDING -eq 1 ]; then 
	NFILES=20
fi
if [ $DOAUAU -eq 0 ]; then #pp data
	NFILES=50 #how many files merge into one batch
	if [ $DOEMBEDDING -eq 1 ]; then 
		NFILES=8 #how many files merge into one batch
	fi
fi
#if [ $ALEXPICO -eq 1 ]; then
#	NFILES=1
#fi

if [ $DOQA -eq 1 ]; then
	NFILES=$((${NFILES}*2))
fi

  	export OUT_PATH=$SCRATCH
  	OUT_PATH_FINAL="${OUTDIR}/${TYPE}"

  	if [ ! -e $OUT_PATH_FINAL ]; then
   	mkdir -p $OUT_PATH_FINAL
	fi

	FILES_PER_HOUR=50
	if [ $DOQA -eq 1 ]; then
		FILES_PER_HOUR=100
	elif [ $DOEMBEDDING -eq 1 ]; then
		FILES_PER_HOUR=10
	fi

	if [ ! -e tmp ]; then
		mkdir -p tmp
	fi
	#rm tmp/*

#===========================
#create submission xml file
#===========================
echo "<?xml version=\"1.0\" encoding=\"utf-8\" ?>" > tmp/$TEMPLATE_NAME
echo "<job maxFilesPerProcess=\"$NFILES\" simulateSubmission = \"false\" filesPerHour = \"$FILES_PER_HOUR\">" >> tmp/$TEMPLATE_NAME
echo "<command>" >> tmp/$TEMPLATE_NAME
echo setenv TRIGGER $TRIGGER >> tmp/$TEMPLATE_NAME
echo setenv DOAUAU $DOAUAU >> tmp/$TEMPLATE_NAME
echo setenv DOQA $DOQA >> tmp/$TEMPLATE_NAME
echo setenv SAVETREE $SAVETREE >> tmp/$TEMPLATE_NAME
echo setenv DOEMBEDDING $DOEMBEDDING >> tmp/$TEMPLATE_NAME
echo setenv PYTHIAEMB $PYTHIAEMB >> tmp/$TEMPLATE_NAME
echo setenv PARTON $PARTON >> tmp/$TEMPLATE_NAME
echo setenv DOEVENTCUTS $DOEVENTCUTS >> tmp/$TEMPLATE_NAME
echo setenv GLOBAL $GLOBAL >> tmp/$TEMPLATE_NAME
echo setenv RRHO $RRHO >> tmp/$TEMPLATE_NAME
echo setenv ZVERTEX $ZVERTEX >> tmp/$TEMPLATE_NAME
echo setenv ZTPCZVPD $ZTPCZVPD >> tmp/$TEMPLATE_NAME
echo setenv TOFBEMC $TOFBEMC >> tmp/$TEMPLATE_NAME
echo setenv BBCMIN $BBCMIN >> tmp/$TEMPLATE_NAME
echo setenv BBCMAX $BBCMAX >> tmp/$TEMPLATE_NAME
echo setenv MAXRAP $MAXRAP >> tmp/$TEMPLATE_NAME
echo setenv DCA $DCA >> tmp/$TEMPLATE_NAME
echo setenv CHI2 $CHI2 >> tmp/$TEMPLATE_NAME
echo setenv NFIT $NFIT >> tmp/$TEMPLATE_NAME
echo setenv NFITNMAX $NFITNMAX >> tmp/$TEMPLATE_NAME
echo setenv ALEXPICO $ALEXPICO >> tmp/$TEMPLATE_NAME
echo setenv NEVENTS $NEVENTS >> tmp/$TEMPLATE_NAME
echo setenv CENTRAL $CENTRAL>> tmp/$TEMPLATE_NAME
echo setenv REFMULTMIN $REFMULTMIN >> tmp/$TEMPLATE_NAME
echo setenv REFMULTMAX $REFMULTMAX >> tmp/$TEMPLATE_NAME
echo setenv NJETSREMOVE $NJETSREMOVE >> tmp/$TEMPLATE_NAME
#echo setenv IN_FILELIST $IN_FILELIST >> tmp/$TEMPLATE_NAME
echo setenv MACRODIR $MACRODIR >> tmp/$TEMPLATE_NAME
echo setenv OUT_PATH $OUT_PATH >> tmp/$TEMPLATE_NAME
echo "  starver $STARLIB_VER" >> tmp/$TEMPLATE_NAME
echo "  source $ANALYSISDIR/set_paths.csh" >> tmp/$TEMPLATE_NAME
echo "  cd $ANALYSISDIR" >> tmp/$TEMPLATE_NAME
echo "  pwd" >> tmp/$TEMPLATE_NAME
echo "  root4star -q -b -l run_analysis.C\($NEVENTS,0\)" >> tmp/$TEMPLATE_NAME
echo "  </command>" >> tmp/$TEMPLATE_NAME
echo "  <stdout URL=\"file:$LOGDIR/\$JOBID.log\"/>" >> tmp/$TEMPLATE_NAME
echo "  <stderr URL=\"file:$ERRDIR/\$JOBID.err\"/>" >> tmp/$TEMPLATE_NAME
echo "  <input URL=\"filelist:$IN_FILELIST\"/>" >> tmp/$TEMPLATE_NAME
echo "  <output fromScratch=\"*.root\" toURL=\"file:$OUT_PATH_FINAL/\"/>" >> tmp/$TEMPLATE_NAME
echo "  <SandBox>" >> tmp/$TEMPLATE_NAME
echo "    <Package>" >> tmp/$TEMPLATE_NAME
echo "	      <File>file:$ANALYSISDIR/run_analysis.C</File>" >> tmp/$TEMPLATE_NAME
echo "			  </Package>" >> tmp/$TEMPLATE_NAME
echo "			  </SandBox>" >> tmp/$TEMPLATE_NAME
echo "			  </job>" >> tmp/$TEMPLATE_NAME

#let's submit
	cd tmp
	star-submit $TEMPLATE_NAME 
	cd ..

done #loop over centralities

