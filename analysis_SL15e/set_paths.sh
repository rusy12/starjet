#!/bin/bash
STARLIB_VER="SL15e_embed"
BASEPATH="/star/u/rusnak/JET_analysis_run11/STARJet"
export STARJETBASEDIR="$BASEPATH" 
export ANALYSISDIR="$BASEPATH/analysis_$STARLIB_VER"
export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$ANALYSISDIR/lib"

export ROOUNFOLD="$BASEPATH/software_$STARLIB_VER/RooUnfold/v-trunk-custom"
export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$ROOUNFOLD"

export FASTJETDIR="$BASEPATH/software_$STARLIB_VER/fastjet3"
export PATH="$PATH:$FASTJETDIR/bin"
export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$FASTJETDIR/lib"

export TOYMODELDIR="/star/u/rusnak/JET_analysis_run11/toymodel"
